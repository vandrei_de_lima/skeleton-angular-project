import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterOutlet } from '@angular/router';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { BrowserModule } from '@angular/platform-browser';
import { TranslateModule, TranslateService } from '@ngx-translate/core';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'skeleton';

  constructor(private translate: TranslateService) {
    translate.setDefaultLang('en');

    const browserLang = translate.getBrowserLang();
    if (browserLang)
      translate.use(browserLang.match(/en|pt-br/) ? browserLang : 'pt-br');
    else {
      translate.use('pt-br');
    }
  }
}
