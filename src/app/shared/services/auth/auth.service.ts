import { Injectable, inject, signal } from '@angular/core';
import {
  Auth,
  user,
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  User,
  sendPasswordResetEmail,
  signInWithPopup,
} from '@angular/fire/auth';
import * as auth from 'firebase/auth';
import { from } from 'rxjs';
@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private auth: Auth = inject(Auth);
  user$ = user(this.auth);

  isAdmin = signal(false);

  user!: User;

  constructor() {
    this.user$.subscribe((result) => {
      this.isAdmin.update((boolean) => !!!!result?.uid);
      if (result) this.user = result;
    });
  }

  createUser(email: string, pasword: string) {
    return from(createUserWithEmailAndPassword(this.auth, email, pasword));
  }

  login(email: string, pasword: string, isPersistence = false) {
    const persistence = isPersistence
      ? auth.browserSessionPersistence
      : auth.browserLocalPersistence;

    this.auth.setPersistence(persistence);
    return from(signInWithEmailAndPassword(this.auth, email, pasword));
  }

  foregetPassword(email: string) {
    return from(
      sendPasswordResetEmail(this.auth, email).catch((error) => {
        alert(error);
      })
    );
  }

  googleAuth() {
    return this.authLogin(new auth.GoogleAuthProvider());
  }

  private authLogin(provider: any) {
    return from(
      signInWithPopup(this.auth, provider)
        .then(() => {
          console.log('Logou com sucesso');
        })
        .catch((error) => {
          window.alert(error);
        })
    );
  }
}
